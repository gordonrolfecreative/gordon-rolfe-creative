**Website & Content Management**
Sounds like a mouthful right? Website and content management is a service designed for people who already have a website and are just looking to keep it fresh, with up to date meaningful content. This product is intended to be used by people who just don’t want to have to update their website themselves because they don’t know how or don’t have time.

I can work with a number of different Content Management Systems (CMS) Like WordPress, Joomla & Drupal. If your website is built with plain PHP or HTML, I can work with that too, these kinds of changes are often one offs. Contact me & lets have a chat about how I can help manage your websites content and keep that top position in Google!

[ Get In Touch with Gordon-Rolfe | Creative to find out more!](https://www.gordon-rolfe.uk/contact)

---

## Social Media Marketing & Management

Getting Social Media right can be tricky! If its done brilliantly, it can really get business booming and instil confidence in your customers! If its done badly on the other hand, it can have drastic impacts to your business and customer confidence!

I have Several years experience looking after brand social media, from creating the initial profiles & branding them inline with what your customers expect right the way through to getting the schedule for posts just right & optimising every word so that your posts really grab your customers attention.

Choosing which social networks to use out of the big ones can be tricky, it’s really all about what your business does & how your customers are likely to ‘consume media’. Give me a shout & I can help you get everything your business needs just right!

[ Check out Gordon-Rolfe | Creative's blog to find out more!](https://www.gordon-rolfe.uk/blog)

---

## Online Presence Creation & Management

It doesn’t matter if you just need some nice imagery to show your brand off on social networks or you need a complete E-Commerce site with multinational options setup, I can help you get your business online & get you noticed! I have a number of years experience working with clients of all shapes and sizes doing everything from PC Repair to Baking Cakes, from Dog Grooming to Guest Houses! If your business needs something digital to help you get noticed. I will definitely be able to help you!

From as little as £99* one off (plus annual hosting fees starting at £24.99 a year) I can get your business online with a stylish website with up to 4 static pages (that means they don’t change)! Starting from £299* one off (plus annual hosting fees starting at £24.99) I can build you a dynamic website or blog so that your business can really hit the nail on the head when it comes to user & customer engagement!

PLEASE NOTE: 
*all prices are provided for illustrative purposes only, for a quote, please Get In Touch.

[Gordon-Rolfe | Creative - Web Design, Social Marketing & Content Management](https://www.gordon-rolfe.uk) is your one stop small business shop to getting things going online!
